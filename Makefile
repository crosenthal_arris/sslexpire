ALL = sslexpire sslexpire.conf

SBIN = /usr/local/sbin
ETC = /usr/local/etc

INSTALL = $(SBIN)/sslexpire $(ETC)/sslexpire.conf

all : $(ALL)

install : $(INSTALL)

$(SBIN)/sslexpire : sslexpire
	install -m 555 $< $@

$(ETC)/sslexpire.conf : sslexpire.conf
	install -m 444 $< $@

